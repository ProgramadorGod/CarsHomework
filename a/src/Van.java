import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Van extends Vehicles{
    private float ChargeVolume;

    public ArrayList<String> VanSet() throws IllegalAccessException {
        ArrayList<String> DatesList = new ArrayList<>();
        Scanner in = new Scanner(System.in);
        Vehicles a = new Vehicles();
        ArrayList<Field> fields = new ArrayList<>(Arrays.asList(getClass().getDeclaredFields()));
        for (Field field:fields) {
            DatesList.add(field.getName() + ": "+ field.get(this));
        }

        Class<?> patClass = a.getClass();
        Field[] patFields = patClass.getDeclaredFields();
        for (Field patField: patFields) {
            DatesList.add(patField.getName() + ": " + patField.get(a));
        }
        return DatesList;
    }

    public Van() throws IllegalAccessException {
    }
}
